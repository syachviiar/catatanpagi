import 'package:flutter/material.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Catatan Pagi"),
        ),
        body: MyForm(),
      ),
    );
  }
}

class MyForm extends StatefulWidget {
  @override
  _MyFormState createState() => _MyFormState();
}

class _MyFormState extends State<MyForm> {
  TextEditingController _judulController = TextEditingController();
  TextEditingController _catatanController = TextEditingController();
  List<String> CatatanList = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: _judulController,
          decoration: InputDecoration(labelText: "Judul"),
        ),
        TextField(
          controller: _catatanController,
          decoration: InputDecoration(labelText: "Catatan"),
        ),
        Row(
          children: [
            ElevatedButton(
              onPressed: () {
                setState(() {
                  _judulController.text = "";
                  _catatanController.text = "";
                });
              },
              child: Text("Clear"),
            ),
            Spacer(),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  String Judul = _judulController.text;
                  String Catatan = _catatanController.text;
                  String hasil = """$Judul, 
$Catatan""";
                  CatatanList.add(hasil);
                  _judulController.text = "";
                  _catatanController.text = "";
                });
              },
              child: Text("Submit"),
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
            itemCount: CatatanList.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(CatatanList[index]),
              );
            },
          ),
        ),
      ],
    );
  }
}
